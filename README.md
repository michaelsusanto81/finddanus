[![pipeline status](https://gitlab.com/michaelsusanto81/finddanus/badges/master/pipeline.svg)](https://gitlab.com/michaelsusanto81/finddanus/-/commits/master)
[![coverage report](https://gitlab.com/michaelsusanto81/finddanus/badges/master/coverage.svg)](https://gitlab.com/michaelsusanto81/finddanus/-/commits/master)

# Find Danus
We’re here to facilitate organizations or committees to increase their income from selling Foods or Drinks.

This platform also facilitates sellers who don’t know where to sell their products, especially foods and drinks. So, they can also increase their income. 

## Features
* Administration (Login & Register)
```concept
A feature for user to create account and login
to use this app.
```
Implemented by:
*Steven Ciayadi - 1806205104*

* Seeker
```concept
A feature for user to search foods or drinks (Danus)
for their organization to increase their income by 
reselling products.
```
Implemented by:
*Muhammad Rifqi - 1806205621*

* Seller
```concept
A feature for seller to provide products to the seeker.
```
Implemented by:
*Michael Susanto - 1806205653*

* Chat
```concept
A feature for seeker and seller to communicate each other.
```
Implemented by:
*Giffari Faqih Phrasya Hardani - 1806205634*

## Description
FindDanus will be consisted of 5 different services:
* API gateway
    * API gateway service routes all requests to other services.
* Administration
    * Administration service takes care of user registrations and login.
* Seeker
    * Seeker service handles all seeker's functionalities.
* Seller
    * Seller service handles all seller's functionalities.
* Chat
    * Chat service handles chat's functionalities.
    
The project itself will have 3 modules:
* finddanus-api
    * A module that abstracts interface for all services.
* finddanus-domain
    * A module for all business logic used by services.
* finddanus-monolith **(currently only this module exists)**
    * An entry point for FindDanus's monolithic app, including views and controllers.

# How to run
* Clone this repository
```cmd
git clone https://gitlab.com/michaelsusanto81/finddanus.git
```

* Open this project in your IDE (Intellij IDEA is recommended).

* You can tick enable auto-import if you want, but you can do it manually by click "Import changes" in the bottom-right after you opened your project.

* Use default gradle wrapper and click open.

* Wait Intellij IDEA to build the project.

* Go to finddanus-monolith/src/main/java/com/finddanus/FindDanusApplication.java and click run.

* Open localhost:8080