package com.finddanus.seller.service;

import static org.junit.jupiter.api.Assertions.*;

import com.finddanus.FindDanusApplication;
import com.finddanus.chat.model.Chat;
import com.finddanus.seeker.model.Observer;
import com.finddanus.seeker.model.Seeker;
import com.finddanus.seeker.model.SeekerBuilder;
import com.finddanus.seller.model.Product;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.model.SellerBuilder;
import com.finddanus.seller.repository.ProductRepository;
import com.finddanus.seller.repository.SellerRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@SpringBootTest(classes = FindDanusApplication.class)
public class SellerServiceImplTest {

  @Autowired
  private SellerRepository sellerRepository;

  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private SellerServiceImpl sellerService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  private Seller seller;
  private Seller seller2;
  private Seeker seeker;
  private Product product;
  private List<Chat> chats;
  private List<Observer> followers;
  private List<Product> products;

  /**
   * Init dependencies.
   */
  @BeforeEach
  public void setUp() {
    chats = new ArrayList<>();
    followers = new ArrayList<>();
    products = new ArrayList<>();
    seller = SellerBuilder.createSeller()
        .withId(1)
        .withName("Michael")
        .withEmail("michael@gmail.com")
        .withPassword(passwordEncoder.encode("12345678"))
        .withLocation("Jakarta")
        .withPhoneNum("081234567890")
        .withChats(chats)
        .withFollowers(followers)
        .withProducts(products)
        .build();
    seller2 = SellerBuilder.createSeller()
        .withId(11)
        .withName("Michael2")
        .withEmail("michael2@gmail.com")
        .withPassword(passwordEncoder.encode("123456782"))
        .withLocation("Jakarta2")
        .withPhoneNum("0812345678902")
        .withChats(chats)
        .withFollowers(followers)
        .withProducts(products)
        .build();
    seeker = SeekerBuilder.createSeeker()
        .withId(1)
        .withName("Rifqi")
        .withEmail("rifqi@gmail.com")
        .withPassword(passwordEncoder.encode("cobaCoba0808"))
        .withLocation("Bogor")
        .withPhoneNum("080808080808")
        .withChats(chats)
        .withFollowing(new ArrayList<>())
        .build();
    product = new Product(1, "risol", "risol enak", 1000, seller);
  }

  @Test
  public void testFindAllSellers() {
    assertNotNull(sellerService.findAllSellers().size());
  }

  @Test
  public void testFindSeller() {
    sellerService.addSeller(seller);
    Optional<Seller> sellerIfExist = sellerService.findSeller(1);
    assertNotNull(sellerIfExist.get());
  }

  @Test
  public void testRemoveSeller() {
    sellerRepository.save(seller);
    try {
      sellerService.removeSeller(1);
    } catch (Exception e) {
      assertNotNull(sellerService.findAllSellers().size());
    }
  }

  @Test
  public void testAddSeller() {
    Seller addedSeller = sellerService.addSeller(seller);
    assertNotNull(sellerService.findAllSellers().size());
  }

  @Test
  public void testEditProfileFail() {
    Seller addedSeller = sellerService.addSeller(seller);
    Seller editedSeller = sellerService.editProfile(
        1,
        "gallan",
        "gallan@gmail.com",
        "Depok",
        "082308230823",
        "asd");
  }

  @Test
  public void testEditProfileSuccess() {
    Seller addedSeller = sellerService.addSeller(seller);
    Seller editedSeller = sellerService.editProfile(
        1,
        "gallan",
        "gallan@gmail.com",
        "Depok",
        "082308230823",
        "12345678");
    assertNotNull(sellerService.findSeller(1).get().getName());
  }

  @Test
  public void testEditPasswordFail() {
    Seller addedSeller = sellerService.addSeller(seller);
    Seller editedSeller = sellerService.editPassword(
        1,
        "12345678",
        "asd",
        "asdasd");
    assertNotNull(sellerService.findSeller(1).get().getPassword());
  }

  @Test
  public void testEditPasswordSuccess() {
    Seller addedSeller = sellerService.addSeller(seller);
    Seller editedSeller = sellerService.editPassword(
        1,
        "12345678",
        "asdasd",
        "asdasd");
    assertNotNull(sellerService.findSeller(1).get().getPassword());
  }

  @Test
  public void testFindAllProducts() {
    try {
      productRepository.save(product);
    } catch (Exception e) {
      assertNotNull(sellerService.findAllProducts().size());
    }
  }

  @Test
  public void testFindProductsBySellerId() {
    try {
      productRepository.save(product);
    } catch (Exception e) {
      assertNotNull(sellerService.findProductsBySellerId(1));
    }
  }

  @Test
  public void testFindProduct() {
    sellerService.addProduct(product);
    Optional<Product> productIfExist = sellerService.findProduct(1);
  }

  @Test
  public void testRemoveProduct() {
    sellerService.addProduct(product);
    try{
      sellerService.removeProduct(1);
    } catch(Exception e) {}
    assertNotNull(sellerService.findAllProducts().size());
  }

  @Test
  public void testAddProduct() {
    sellerService.addProduct(product);
    assertNotNull(sellerService.findAllProducts().size());
  }

  @Test
  public void testEditProduct() {
    try {
      sellerService.editProduct(product);
    } catch (Exception e) {
      assertNotNull(sellerService.findAllProducts().size());
    }
  }

  @Test
  public void testGetFollowersFail() {
    List<Observer> seekers = sellerService.getFollowers(11);
    assertNull(seekers);
  }

  @Test
  public void testGetFollowersSuccess() {
    sellerRepository.save(seller);
    List<Observer> seekers = sellerService.getFollowers(1);
    assertNotNull(seekers);
  }

  @Test
  public void testGetChats() {
    List<Chat> chats = sellerService.getChats(1);
    assertNotNull(chats);
  }

  @Test
  public void testGetChatsFail() {
    List<Chat> chats = sellerService.getChats(100);
    assertNull(chats);
  }
}
