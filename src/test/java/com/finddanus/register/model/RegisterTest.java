package com.finddanus.register.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RegisterTest {

    private Register register;
    @BeforeEach
    public void setUp() {
        register = new Register("Steven", "steven@gmail.com", "12345678", "Jakarta", "082295570585");
    }

    @Test
    public void testGetName() {
        assertEquals("Steven", register.getName());
    }

    @Test
    public void testSetName() {
        register.setName("Bambang");
        assertEquals("Bambang", register.getName());
    }

    @Test
    public void testGetEmail() {
        assertEquals("steven@gmail.com", register.getEmail());
    }

    @Test
    public void testSetEmail() {
        register.setEmail("bambang@gmail.com");
        assertEquals("bambang@gmail.com", register.getEmail());
    }

    @Test
    public void testGetPassword() {
        assertEquals("12345678", register.getPassword());
    }

    @Test
    public void testSetPassword() {
        register.setPassword("87654321");
        assertEquals("87654321", register.getPassword());
    }

    @Test
    public void testGetLocation() {
        assertEquals("Jakarta", register.getLocation());
    }

    @Test
    public void testSetLocation() {
        register.setLocation("Bogor");
        assertEquals("Bogor", register.getLocation());
    }

    @Test
    public void testGetPhoneNum() {
        assertEquals("082295570585", register.getPhoneNum());
    }

    @Test
    public void testSetPhoneNum() {
        register.setPhoneNum("081211223344");
        assertEquals("081211223344", register.getPhoneNum());
    }
}
