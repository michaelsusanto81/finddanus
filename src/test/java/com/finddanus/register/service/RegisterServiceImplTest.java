package com.finddanus.register.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.finddanus.seeker.model.Seeker;
import com.finddanus.seeker.repository.SeekerRepository;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.repository.SellerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
public class RegisterServiceImplTest {

  @Mock
  private SellerRepository sellerRepository;

  @Mock
  private SeekerRepository seekerRepository;

  @Mock
  private PasswordEncoder passwordEncoder;

  @InjectMocks
  private RegisterServiceImpl registerService;

  @Test
  public void testCreateSeekerFail() {
    Seeker editedSeeker = registerService.createSeeker(
        "gallan",
        "gallan@gmail.com",
        "Depok",
        "082308230823",
        "asd",
        "dsa");
    verify(seekerRepository, times(0)).save(editedSeeker);
  }

  @Test
  public void testCreateSellerFail() {
    Seller editedSeller = registerService.createSeller(
        "gallan",
        "gallan@gmail.com",
        "Depok",
        "082308230823",
        "asd",
        "dsa");
    verify(sellerRepository, times(0)).save(editedSeller);
  }

  @Test
  public void testCreateSeeker() {
    Seeker editedSeeker = registerService.createSeeker(
        "gallan",
        "gallan@gmail.com",
        "Depok",
        "082308230823",
        "asd",
        "asd");
    verify(seekerRepository, times(0)).save(editedSeeker);
  }

  @Test
  public void testCreateSeller() {
    Seller editedSeller = registerService.createSeller(
        "gallan",
        "gallan@gmail.com",
        "Depok",
        "082308230823",
        "asd",
        "asd");
    verify(sellerRepository, times(0)).save(editedSeller);
  }

}