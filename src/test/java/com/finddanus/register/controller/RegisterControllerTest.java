package com.finddanus.register.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.finddanus.register.model.Register;
import com.finddanus.register.service.RegisterServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@WebMvcTest(controllers = RegisterController.class)
public class RegisterControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private Register register;

  @MockBean
  private RegisterServiceImpl registerService;

  @Test
  public void testGetAllRegister() throws Exception {
    mockMvc.perform(get("/register"))
        .andExpect(status().isOk());
  }

  @Test
  public void testEditAccountSellerNotFound() throws Exception {
    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("name", "test");
    params.add("email", "test@gmail.com");
    params.add("location", "Jakarta");
    params.add("phoneNum", "081212121212");
    params.add("password", "seller1");
    params.add("passwordConfirm", "seller1");
    params.add("type", "seller");

    mockMvc.perform(post("/register").params(params))
        .andExpect(handler().methodName("createAccount"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/register"));
  }

  @Test
  public void testEditAccountSeekerNotFound() throws Exception {
    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("name", "test");
    params.add("email", "test@gmail.com");
    params.add("location", "Jakarta");
    params.add("phoneNum", "081212121212");
    params.add("password", "seller1");
    params.add("passwordConfirm", "seller1");
    params.add("type", "seeker");

    mockMvc.perform(post("/register").params(params))
        .andExpect(handler().methodName("createAccount"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/register"));
  }

}
