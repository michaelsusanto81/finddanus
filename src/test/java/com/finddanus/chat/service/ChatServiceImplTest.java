package com.finddanus.chat.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

import com.finddanus.FindDanusApplication;
import com.finddanus.chat.model.Chat;
import com.finddanus.chat.repository.ChatRepository;
import com.finddanus.chat.repository.MessageRepository;
import com.finddanus.seeker.model.Observer;
import com.finddanus.seeker.model.Seeker;
import com.finddanus.seeker.model.SeekerBuilder;
import com.finddanus.seeker.service.SeekerService;
import com.finddanus.seller.model.Product;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.model.SellerBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.util.ArrayList;
import java.util.List;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@SpringBootTest(classes = FindDanusApplication.class)
public class ChatServiceImplTest {

  @Autowired
  private ChatRepository chatRepository;

  @Autowired
  private MessageRepository messageRepository;

  @Autowired
  private SeekerService seekerService;

  @Autowired
  private ChatServiceImpl chatServiceImpl;

  private Seller seller;
  private Seller seller2;
  private Seeker seeker;
  private Chat chat;
  private List<Chat> chats;
  private List<Observer> followers;
  private List<Product> products;

  /**
   * Init dependencies.
   */
  @BeforeEach
  public void setUp() {
    chats = new ArrayList<>();
    followers = new ArrayList<>();
    products = new ArrayList<>();
    seller = SellerBuilder.createSeller()
        .withId(1)
        .withName("Michael")
        .withEmail("michael@gmail.com")
        .withPassword("12345678")
        .withLocation("Jakarta")
        .withPhoneNum("081234567890")
        .withChats(chats)
        .withFollowers(followers)
        .withProducts(products)
        .build();
    seller2 = SellerBuilder.createSeller()
        .withId(11)
        .withName("Michael2")
        .withEmail("michael2@gmail.com")
        .withPassword("123456782")
        .withLocation("Jakarta2")
        .withPhoneNum("0812345678902")
        .withChats(chats)
        .withFollowers(followers)
        .withProducts(products)
        .build();
    seeker = SeekerBuilder.createSeeker()
        .withId(1)
        .withName("Rifqi")
        .withEmail("rifqi@gmail.com")
        .withPassword("cobaCoba0808")
        .withLocation("Bogor")
        .withPhoneNum("080808080808")
        .withChats(chats)
        .withFollowing(new ArrayList<>())
        .build();
    chat = new Chat(seeker, seller);
  }

  @Test
  public void testFindChatById(){ assertNotNull(chatServiceImpl.findChatById(chat.getId())); }

  @Test
  public void testAddSeekerMessage(){
    chat = chatServiceImpl.addSeekerMessage(1, seeker.getId(), "halo");
    assertNull(chat);
  }

  @Test
  public void testAddSellerMessage(){
    chat = chatServiceImpl.addSellerMessage(1, seller.getId(), "halo");
    assertNull(chat);
  }
}
