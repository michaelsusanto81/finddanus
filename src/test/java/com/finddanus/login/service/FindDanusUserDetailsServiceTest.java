package com.finddanus.login.service;

import com.finddanus.seeker.repository.SeekerRepository;
import com.finddanus.seller.repository.SellerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class FindDanusUserDetailsServiceTest {

  @InjectMocks
  private FindDanusUserDetailsService userDetailsService;

  @Mock
  private SeekerRepository seekerRepository;

  @Mock
  private SellerRepository sellerRepository;

  @Test
  public void testNotFound() {
    assertThrows(UsernameNotFoundException.class,
        () -> userDetailsService.loadUserByUsername("michael@gmail.com"));
  }
}
