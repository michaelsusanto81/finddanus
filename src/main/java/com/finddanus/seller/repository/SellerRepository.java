package com.finddanus.seller.repository;

import com.finddanus.seller.model.Seller;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SellerRepository extends JpaRepository<Seller, Long> {

  @Query(
      value = "SELECT S.seeker_id, S.email, S.location, S.name, S.password, S.phone_num "
      + "FROM SEEKER_SELLER SS JOIN SEEKER S ON SS.seeker_id = S.seeker_id WHERE SS.seller_id = ?1",
      nativeQuery = true
  )
  List<Long> getFollowers(long sellerId);

  Optional<Seller> findByEmail(String email);
}
