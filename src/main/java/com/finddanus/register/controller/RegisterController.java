package com.finddanus.register.controller;

import com.finddanus.register.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RegisterController {

  @Autowired
  private final RegisterService registerService;

  public RegisterController(RegisterService registerService) {
    this.registerService = registerService;
  }

  @GetMapping("/register")
  public String register() {
    return "register";
  }

  /**
   * Register endpoint.
   * @param name of user
   * @param email of user
   * @param location of user
   * @param phoneNum of user
   * @param password of user
   * @param passwordConfirm of user
   * @param type of user
   * @return redirect to /register
   */
  @PostMapping("/register")
  public String createAccount(
      @RequestParam(value = "name") String name,
      @RequestParam(value = "email") String email,
      @RequestParam(value = "location") String location,
      @RequestParam(value = "phoneNum") String phoneNum,
      @RequestParam(value = "password") String password,
      @RequestParam(value = "passwordConfirm") String passwordConfirm,
      @RequestParam(value = "type") String type
  ) {
    if (type.equalsIgnoreCase("seeker")) {
      registerService.createSeeker(name, email, location, phoneNum, password, passwordConfirm);
    } else {
      registerService.createSeller(name, email, location, phoneNum, password, passwordConfirm);
    }
    return "redirect:/register";
  }
}
