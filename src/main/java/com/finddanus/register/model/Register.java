package com.finddanus.register.model;

public class Register {
  private String name;
  private String email;
  private String password;
  private String location;
  private String phoneNum;

  /**
   * Register DTO.
   * @param name of user
   * @param email of user
   * @param password of user
   * @param location of user
   * @param phoneNum of user
   */
  public Register(String name, String email, String password, String location, String phoneNum) {
    this.name = name;
    this.email = email;
    this.password = password;
    this.location = location;
    this.phoneNum = phoneNum;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getPhoneNum() {
    return phoneNum;
  }

  public void setPhoneNum(String phoneNum) {
    this.phoneNum = phoneNum;
  }
}
