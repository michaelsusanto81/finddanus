package com.finddanus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FindDanusApplication {
  public static void main(String[] args) {
    SpringApplication.run(FindDanusApplication.class, args);
  }
}
