package com.finddanus.seeker.model;

import com.finddanus.chat.model.Chat;
import com.finddanus.seller.model.Subject;
import java.util.List;

public final class SeekerBuilder {
  private long id;
  private String name;
  private String email;
  private String password;
  private String location;
  private String phoneNum;
  private List<Chat> chats;
  private List<Subject> following;

  private SeekerBuilder() {
  }

  public static SeekerBuilder createSeeker() {
    return new SeekerBuilder();
  }

  public Seeker build() {
    return new Seeker(this);
  }

  public SeekerBuilder withId(long id) {
    this.id = id;
    return this;
  }

  public SeekerBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public SeekerBuilder withEmail(String email) {
    this.email = email;
    return this;
  }

  public SeekerBuilder withPassword(String password) {
    this.password = password;
    return this;
  }

  public SeekerBuilder withLocation(String location) {
    this.location = location;
    return this;
  }

  public SeekerBuilder withPhoneNum(String phoneNum) {
    this.phoneNum = phoneNum;
    return this;
  }

  public SeekerBuilder withChats(List<Chat> chats) {
    this.chats = chats;
    return this;
  }

  public SeekerBuilder withFollowing(List<Subject> following) {
    this.following = following;
    return this;
  }

  public long getId() {
    return this.id;
  }

  public String getName() {
    return this.name;
  }

  public String getEmail() {
    return this.email;
  }

  public String getPassword() {
    return this.password;
  }

  public String getLocation() {
    return this.location;
  }

  public String getPhoneNum() {
    return this.phoneNum;
  }

  public List<Chat> getChats() {
    return this.chats;
  }

  public List<Subject> getFollowing() {
    return this.following;
  }
}