package com.finddanus.seeker.controller;

import com.finddanus.chat.model.Chat;
import com.finddanus.login.model.FindDanusUserDetails;
import com.finddanus.seeker.model.Seeker;
import com.finddanus.seeker.service.SeekerService;
import com.finddanus.seller.model.Product;
import com.finddanus.seller.model.Subject;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class SeekerController {

  @Autowired
  private SeekerService seekerService;

  public SeekerController(SeekerService seekerService) {
    this.seekerService = seekerService;
  }

  /**
   * Returns seeker's homepage.
   * @param model for the view
   * @return seeker/seekerHomepage.html
   */
  @GetMapping(path = "/home")
  public String seekerHomepage(Model model, HttpServletRequest request, Authentication auth) {
    long id = ((FindDanusUserDetails) auth.getPrincipal()).getId();
    request.getSession().setAttribute("id", id);
    List<Product> productsByFollowedSeller = seekerService.showHomepage(id);
    Seeker thisSeeker = seekerService.getSeekerObjectById(id);
    try {
      String thisSeekerLocation = thisSeeker.getLocation();
      model.addAttribute("seekerId", id);
      model.addAttribute("seekerLocation", thisSeekerLocation);
      model.addAttribute("products", productsByFollowedSeller);
    } catch (NullPointerException e) {
      model.addAttribute("notFound", true);
    }
    return "seeker/seekerHomepage";
  }

  /**
   * Returns seeker's profile page.
   * @param model for the view
   * @return seeker/seekerProfile.html
   */
  @GetMapping("/profile")
  public String seekerProfile(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    Seeker seeker = seekerService.getSeekerObjectById(id);
    try {
      model.addAttribute("seekerId", id);
      model.addAttribute("seeker", seeker);
    } catch (NullPointerException e) {
      model.addAttribute("notFound", true);
    }
    return "seeker/seekerProfile";
  }

  @PostMapping("/home")
  public String searchProduct(
      RedirectAttributes redirectAttributes,
      @RequestParam(value = "productName") String productName,
      HttpServletRequest request
  ) {
    redirectAttributes.addAttribute("productName", productName);
    return "redirect:/search/productName={productName}";
  }

  /**
   * Returns products with specific name.
   * @param productName for product's name
   * @param model for the view
   * @return seeker/seekerSearchResult.html
   */
  @GetMapping("/search/productName={productName}")
  public String showProductByName(
      @PathVariable String productName,
      Model model,
      HttpServletRequest request
  ) {
    long id = (long) request.getSession().getAttribute("id");
    List<Product> productsByName = seekerService.showProductByName(productName);
    model.addAttribute("products", productsByName);
    model.addAttribute("keyword", productName);
    model.addAttribute("seekerId", id);
    return "seeker/seekerSearchResult";
  }

  /**
   * Returns products with specific location.
   * @param seekerLocation for product's name
   * @param model for the view
   * @return seeker/seekerSearchResult.html
   */
  @GetMapping("/search/location={seekerLocation}")
  public String showProductByLocation(
      @PathVariable String seekerLocation,
      Model model,
      HttpServletRequest request
  ) {
    long id = (long) request.getSession().getAttribute("id");
    List<Product> productsByLocation = seekerService.showProductByLocation(seekerLocation);
    model.addAttribute("products", productsByLocation);
    model.addAttribute("keyword", seekerLocation);
    model.addAttribute("seekerId", id);
    return "seeker/seekerSearchResult";
  }

  /**
   * Follow Seller then return to previous url.
   * @param sellerId the target seller's id
   * @return redirect to previous url (search results page)
   */
  @GetMapping("/follow/{sellerId}")
  public String seekerFollowSeller(
      @PathVariable long sellerId,
      HttpServletRequest request
  ) {
    long seekerId = (long) request.getSession().getAttribute("id");
    seekerService.followSeller(seekerId, sellerId);
    return "redirect:" + request.getHeader("Referer");
  }

  /**
   * Unfollow Seller then return to previous url.
   * @param sellerId the target seller's id
   * @return redirect to previous url (following page)
   */
  @GetMapping("/unfollow/{sellerId}")
  public String seekerUnfollowSeller(
      @PathVariable long sellerId,
      HttpServletRequest request
  ) {
    long seekerId = (long) request.getSession().getAttribute("id");
    seekerService.unfollowSeller(seekerId, sellerId);
    return "redirect:" + request.getHeader("Referer");
  }

  /**
   * Returns seeker's edit profile page.
   * @param model for the view
   * @return seeker/seekerEdit.html
   */
  @GetMapping("/edit")
  public String editProfilePage(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    Optional<Seeker> seeker = seekerService.findSeeker(id);
    try {
      model.addAttribute("seeker", seeker.get());
      return "seeker/seekerEdit";
    } catch (NoSuchElementException e) {
      return "redirect:/profile";
    }
  }

  /**
   * Returns seeker's following page.
   * @param model for the view
   * @return seeker/seekerFollowing.html
   */
  @GetMapping("/following")
  public String seekerFollowingPage(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    Seeker thisSeeker = seekerService.getSeekerObjectById(id);
    try {
      model.addAttribute("seekerId", id);
      List<Subject> seekerFollowing = thisSeeker.getFollowing();
      model.addAttribute("seekerFollowing", seekerFollowing);
    } catch (NullPointerException e) {
      model.addAttribute("notFound", true);
    }
    return "seeker/seekerFollowing";
  }

  /**
   * Update profile to database.
   * @param seekerId of seeker
   * @param name of seeker
   * @param email of seeker
   * @param location of seeker
   * @param phoneNum of seeker
   * @param passwordConfirm of seeker
   * @return redirect to seeker profile
   */
  @PostMapping("/edit")
  public String updateProfile(
      @RequestParam(value = "seeker_id") long seekerId,
      @RequestParam(value = "name") String name,
      @RequestParam(value = "email") String email,
      @RequestParam(value = "location") String location,
      @RequestParam(value = "phoneNum") String phoneNum,
      @RequestParam(value = "passwordConfirm") String passwordConfirm
  ) {
    if (seekerService.editSeekerProfile(seekerId,
        name,
        email,
        location,
        phoneNum,
        passwordConfirm) != null) {
      return "redirect:/profile";
    }
    return "redirect:/edit";
  }

  /**
   * Update profile to database.
   * @param seekerId of seeker
   * @param oldPassword of seeker
   * @param newPassword of seeker
   * @param confirmNewPassword of seeker
   * @return redirect to seeker profile
   */
  @PostMapping("/editPass")
  public String updatePassword(
      @RequestParam(value = "seeker_id") long seekerId,
      @RequestParam(value = "oldPassword") String oldPassword,
      @RequestParam(value = "newPassword") String newPassword,
      @RequestParam(value = "confirmNewPassword") String confirmNewPassword
  ) {
    if (seekerService.editSeekerPassword(seekerId, oldPassword,
        newPassword, confirmNewPassword) != null) {
      return "redirect:/profile";
    }
    return "redirect:/edit";
  }

  /**
   * Return Chats page.
   * @param model to add List of chat to html
   * @return seeker/seekerChatList.html
   */
  @GetMapping("/chats")
  public String getChats(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    List<Chat> chats = seekerService.getChats(id);
    model.addAttribute("chats", chats);
    return "seeker/seekerChatList";
  }

  /**
   * Return Seeker Profile page for Seller.
   * @param id of Seeker
   * @return seeker/seekerProfilePreview.html
   */
  @GetMapping("/preview/{id}")
  public String previewSeekerProfile(@PathVariable long id, Model model) {
    Seeker seeker = seekerService.getSeekerObjectById(id);
    try {
      model.addAttribute("seekerId", id);
      model.addAttribute("seeker", seeker);
    } catch (NullPointerException e) {
      model.addAttribute("notFound", true);
    }
    return "seeker/seekerProfilePreview";
  }
}
