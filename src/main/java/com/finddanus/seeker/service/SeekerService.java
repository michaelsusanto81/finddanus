package com.finddanus.seeker.service;

import com.finddanus.chat.model.Chat;
import com.finddanus.seeker.model.Seeker;
import com.finddanus.seller.model.Product;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.model.Subject;
import java.util.List;
import java.util.Optional;

public interface SeekerService {
  public void addSeeker(Seeker seeker);

  public Optional<Seeker> findSeeker(long seekerId);

  public Optional<Seller> findSeller(long sellerId);

  public Seeker getSeekerObjectById(long seekerId);

  public Seeker editSeekerProfile(long seekerId, String name, String email,
                                  String location, String phoneNumber, String passwordConfirm);

  public Seeker editSeekerPassword(long seekerId, String oldPassword,
                                   String newPassword, String confirmNewPassword);

  public Seller getSellerObjectById(long sellerId);

  public void followSeller(long seekerId, long sellerId);

  public void unfollowSeller(long seekerId, long sellerId);

  public List<Subject> showFollowing(long seekerId);

  public List<Product> showProductByName(String name);

  public List<Product> showHomepage(long seekerId);

  public List<Product> showProductByLocation(String location);

  public List<Chat> getChats(long seekerId);
}
