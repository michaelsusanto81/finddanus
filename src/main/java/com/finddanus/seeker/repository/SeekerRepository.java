package com.finddanus.seeker.repository;

import com.finddanus.seeker.model.Seeker;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeekerRepository extends JpaRepository<Seeker, Long> {
  Optional<Seeker> findByEmail(String email);
}
