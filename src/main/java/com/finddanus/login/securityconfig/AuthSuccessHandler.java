package com.finddanus.login.securityconfig;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
  @Override
  protected String determineTargetUrl(HttpServletRequest request,
                                      HttpServletResponse response,
                                      Authentication authentication) {
    String role = authentication.getAuthorities().toString();
    String targetUrl = "";
    if (role.contains("SEEKER")) {
      targetUrl = "/home";
    } else if (role.contains("SELLER")) {
      targetUrl = "/seller/home";
    }
    return targetUrl;
  }
}
