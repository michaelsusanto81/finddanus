package com.finddanus.chat.model;

import com.finddanus.seeker.model.Seeker;

public class SeekerMessageGenerator extends MessageGenerator {

  private Seeker seeker;

  public SeekerMessageGenerator(Seeker s) {
    this.seeker = s;
  }

  @Override
  public Message createMessage(String text) {
    Message message = new Message(this.seeker, text);
    return message;
  }
}
