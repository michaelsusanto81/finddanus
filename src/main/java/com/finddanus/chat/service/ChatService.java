package com.finddanus.chat.service;

import com.finddanus.chat.model.Chat;
import java.util.Optional;

public interface ChatService {
  public Optional<Chat> findChatById(long chatId);

  public Chat addSeekerMessage(long chatId, long seekerId, String message);

  public Chat addSellerMessage(long chatId, long sellerId, String message);
}
