$(function(){
    $(".btnFollow").on("click", function(e) {
        var btnCn = this.className.match(/\bbtncn[^\s]+\b/)[0];
        var sellerId = btnCn.slice(5);
        var seekerId = $("#seekerId").val();
        var selectBtn = ".".concat(btnCn);
        var selectTxt = ".".concat('btntxt',sellerId);
        $.ajax({
            contentType: 'application/json',
            data: JSON.stringify({
                "seekerId" : seekerId,
                "sellerId" : sellerId
            }),

            // for deployment (comment this in local)
//            url: "https://finddanus-service-user.herokuapp.com/" + seekerId + "/follow",

            // for development (uncomment this in local)
             url: "http://localhost:8083/" + seekerId + "/follow",

            type: 'post',
            success: function(data){
                $(selectBtn).addClass("disabled");
                $(selectTxt).html("Followed")
            }
        });
    });
});