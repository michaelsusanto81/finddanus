$(function(){
    $("#productName").on("keyup", function(e) {
        var keyword = e.currentTarget.value.toLowerCase();
        var seekerId = $("#seekerId").val();

        $.ajax({
            // for deployment (comment this in local)
//            url: "https://finddanus-service-user.herokuapp.com/" + seekerId + "/product=" + keyword,

            // for development (uncomment this in local)
             url: "http://localhost:8083/" + seekerId + "/product=" + keyword,

            datatype: 'json',
            success: function(data){
                $('#result-header').html('');
                $('#results').html('');
                var result = '';

                for(var i = 0; i < data.length; i++) {
                    if (Number.isInteger(data[i]) !== true) {
                    result +=
                        "<div class='card float-left mx-3 my-3' style='width:19.5rem;'>" +
                            "<div class='card-body'>" +
                                "<h5 class='card-title'>" + data[i].title + "</h5>" +
                                "<p class='card-text'>" + data[i].description + "</p>" +
                                "<div class='d-flex flex-row card-text align-items-center mb-2'>"+
                                    "<i class='fas fa-tags'></i>" +
                                    "<p class='mb-0 ml-1'> IDR " + data[i].price + "</p>" +
                                "</div>" +
                                "<div class='d-flex flex-row card-text align-items-center mb-2'>"+
                                    "<i class='fas fa-search-location'></i>" +
                                    "<p class='card-text'>" + data[i].seller.location + "</p>" +
                                "</div>" +
                                "<div class='d-flex flex-row'></div>" +
                                "<a href='/seller/preview/" +  data[i].seller.id + "' class='btn btn-outline-success mt-2'>" +
                                "<div class='d-flex flex-row align-items-center justify-content-center'>" +
                                    "<i class='fas fa-user-circle'></i>" +
                                    "<p class='mb-0 ml-2'>" + data[i].seller.name + "</p>" +
                                "</div>" +
                                "</a>" +
                                "<a href='/follow/" +  data[i].seller.id + "' class='btn btn-outline-primary mt-2'>" +
                                "<div class='d-flex flex-row align-items-center justify-content-center'>" +
                                    "<i class='fas fa-user-plus'></i>" +
                                    "<p class='mb-0 ml-2'>Follow</p>" +
                                "</div>" +
                                "</a>" +
                            "</div>" +
                        "</div>";
                    }
                }
                $('#results').append(result);
            }
        });
    });
});